package az.ingress.market.repository;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Component
@RequiredArgsConstructor
public class ManagerCustomSpecification<T> implements  Specification<T>  {
    List<SearchCriteria> criteriaList;
    final CustomSpecification<T> customSpecification;

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        customSpecification.setCriteriaList(criteriaList);
        Predicate predicate = customSpecification.toPredicate(root,query,criteriaBuilder);
        for (SearchCriteria criteria : criteriaList) {
            if (criteria.getOperation().equals(SearchOperation.CHILD_EQUAL)) {
                predicate = criteriaBuilder.and(
                        predicate,
                        criteriaBuilder.equal(root.join("phones").get(criteria.getKey()), criteria.getValue())
                );
            }
        }
        return predicate;
    }
}
