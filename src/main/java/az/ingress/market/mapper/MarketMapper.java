package az.ingress.market.mapper;

import az.ingress.market.dto.MarketDto;
import az.ingress.market.model.Market;
import org.mapstruct.Mapper;

import java.util.List;
@Mapper(componentModel = "spring")
public interface MarketMapper {
    MarketDto mapToMarketDto(Market market);
    Market mapToMarket(MarketDto marketDto);
    List<MarketDto> mapToMarketDtoList(List<Market> taskList);
}
