package az.ingress.market.mapper;

import az.ingress.market.dto.BranchDto;
import az.ingress.market.dto.ManagerDto;
import az.ingress.market.dto.PhoneDto;
import az.ingress.market.model.Branch;
import az.ingress.market.model.Manager;
import az.ingress.market.model.Phone;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-11-19T20:19:00+0400",
    comments = "version: 1.5.5.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.4.jar, environment: Java 17.0.9 (Amazon.com Inc.)"
)
@Component
public class BranchMapperImpl implements BranchMapper {

    @Override
    public BranchDto mapToBranchDto(Branch branch) {
        if ( branch == null ) {
            return null;
        }

        BranchDto.BranchDtoBuilder branchDto = BranchDto.builder();

        branchDto.name( branch.getName() );
        branchDto.address( branch.getAddress() );
        branchDto.countOfEmployee( branch.getCountOfEmployee() );
        branchDto.phones( phoneListToPhoneDtoList( branch.getPhones() ) );
        branchDto.manager( managerToManagerDto( branch.getManager() ) );
        branchDto.market( branch.getMarket() );

        return branchDto.build();
    }

    @Override
    public Branch mapToBranch(BranchDto branchDto) {
        if ( branchDto == null ) {
            return null;
        }

        Branch.BranchBuilder branch = Branch.builder();

        branch.name( branchDto.getName() );
        branch.address( branchDto.getAddress() );
        branch.countOfEmployee( branchDto.getCountOfEmployee() );
        branch.phones( phoneDtoListToPhoneList( branchDto.getPhones() ) );
        branch.manager( managerDtoToManager( branchDto.getManager() ) );
        branch.market( branchDto.getMarket() );

        return branch.build();
    }

    @Override
    public List<BranchDto> mapToBranchDtoList(List<Branch> branchList) {
        if ( branchList == null ) {
            return null;
        }

        List<BranchDto> list = new ArrayList<BranchDto>( branchList.size() );
        for ( Branch branch : branchList ) {
            list.add( mapToBranchDto( branch ) );
        }

        return list;
    }

    protected PhoneDto phoneToPhoneDto(Phone phone) {
        if ( phone == null ) {
            return null;
        }

        PhoneDto.PhoneDtoBuilder phoneDto = PhoneDto.builder();

        phoneDto.number( phone.getNumber() );

        return phoneDto.build();
    }

    protected List<PhoneDto> phoneListToPhoneDtoList(List<Phone> list) {
        if ( list == null ) {
            return null;
        }

        List<PhoneDto> list1 = new ArrayList<PhoneDto>( list.size() );
        for ( Phone phone : list ) {
            list1.add( phoneToPhoneDto( phone ) );
        }

        return list1;
    }

    protected ManagerDto managerToManagerDto(Manager manager) {
        if ( manager == null ) {
            return null;
        }

        ManagerDto.ManagerDtoBuilder managerDto = ManagerDto.builder();

        managerDto.name( manager.getName() );
        managerDto.surname( manager.getSurname() );
        managerDto.age( manager.getAge() );
        managerDto.phones( phoneListToPhoneDtoList( manager.getPhones() ) );

        return managerDto.build();
    }

    protected Phone phoneDtoToPhone(PhoneDto phoneDto) {
        if ( phoneDto == null ) {
            return null;
        }

        Phone.PhoneBuilder phone = Phone.builder();

        phone.number( phoneDto.getNumber() );

        return phone.build();
    }

    protected List<Phone> phoneDtoListToPhoneList(List<PhoneDto> list) {
        if ( list == null ) {
            return null;
        }

        List<Phone> list1 = new ArrayList<Phone>( list.size() );
        for ( PhoneDto phoneDto : list ) {
            list1.add( phoneDtoToPhone( phoneDto ) );
        }

        return list1;
    }

    protected Manager managerDtoToManager(ManagerDto managerDto) {
        if ( managerDto == null ) {
            return null;
        }

        Manager.ManagerBuilder manager = Manager.builder();

        manager.name( managerDto.getName() );
        manager.surname( managerDto.getSurname() );
        manager.age( managerDto.getAge() );
        manager.phones( phoneDtoListToPhoneList( managerDto.getPhones() ) );

        return manager.build();
    }
}
