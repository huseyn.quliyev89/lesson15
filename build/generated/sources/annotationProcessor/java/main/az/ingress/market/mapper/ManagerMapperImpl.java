package az.ingress.market.mapper;

import az.ingress.market.dto.ManagerDto;
import az.ingress.market.dto.PhoneDto;
import az.ingress.market.model.Manager;
import az.ingress.market.model.Phone;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-11-26T15:15:48+0400",
    comments = "version: 1.5.5.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.4.jar, environment: Java 17.0.9 (Amazon.com Inc.)"
)
@Component
public class ManagerMapperImpl implements ManagerMapper {

    @Override
    public ManagerDto mapToManagerDto(Manager manager) {
        if ( manager == null ) {
            return null;
        }

        ManagerDto.ManagerDtoBuilder managerDto = ManagerDto.builder();

        managerDto.name( manager.getName() );
        managerDto.surname( manager.getSurname() );
        managerDto.age( manager.getAge() );
        managerDto.phones( phoneListToPhoneDtoList( manager.getPhones() ) );

        return managerDto.build();
    }

    @Override
    public Manager mapToManager(ManagerDto managerDto) {
        if ( managerDto == null ) {
            return null;
        }

        Manager.ManagerBuilder manager = Manager.builder();

        manager.name( managerDto.getName() );
        manager.surname( managerDto.getSurname() );
        manager.age( managerDto.getAge() );
        manager.phones( phoneDtoListToPhoneList( managerDto.getPhones() ) );

        return manager.build();
    }

    @Override
    public List<ManagerDto> mapToManagerDtoList(List<Manager> taskList) {
        if ( taskList == null ) {
            return null;
        }

        List<ManagerDto> list = new ArrayList<ManagerDto>( taskList.size() );
        for ( Manager manager : taskList ) {
            list.add( mapToManagerDto( manager ) );
        }

        return list;
    }

    protected PhoneDto phoneToPhoneDto(Phone phone) {
        if ( phone == null ) {
            return null;
        }

        PhoneDto.PhoneDtoBuilder phoneDto = PhoneDto.builder();

        phoneDto.number( phone.getNumber() );

        return phoneDto.build();
    }

    protected List<PhoneDto> phoneListToPhoneDtoList(List<Phone> list) {
        if ( list == null ) {
            return null;
        }

        List<PhoneDto> list1 = new ArrayList<PhoneDto>( list.size() );
        for ( Phone phone : list ) {
            list1.add( phoneToPhoneDto( phone ) );
        }

        return list1;
    }

    protected Phone phoneDtoToPhone(PhoneDto phoneDto) {
        if ( phoneDto == null ) {
            return null;
        }

        Phone.PhoneBuilder phone = Phone.builder();

        phone.number( phoneDto.getNumber() );

        return phone.build();
    }

    protected List<Phone> phoneDtoListToPhoneList(List<PhoneDto> list) {
        if ( list == null ) {
            return null;
        }

        List<Phone> list1 = new ArrayList<Phone>( list.size() );
        for ( PhoneDto phoneDto : list ) {
            list1.add( phoneDtoToPhone( phoneDto ) );
        }

        return list1;
    }
}
